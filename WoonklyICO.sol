

import './zeppelin_template.sol';

pragma solidity 0.4.23;



contract woonkly is StandardToken{
    
    
    /*Basic ERC20 Constants*/
    string public constant name = "Woonk";
    string public constant symbol = "WNK";
    uint  public constant decimals    = 18; 
    
    uint  public  totalUsed   = 0;
    uint  public etherRaised = 0;

    
    /*Price*/
    
    uint256 public constant price= 50000; //current price * 1ETH

    /*  pools  */

   address public constant saleManager = 0x87A7cF4F29b690CA9766316CDd6246994FAE693f; //admin of sale
   address public constant team = 0xCad2Ab8017328C6C032e3DB3ae33539b90B658C9; // founders, private investors and advisors
   address public constant development = 0xE2c97299b8e9d5e8d8D635EE0b28BDf923dE784a; // pool dev
   address public constant community = 0xF868557288d593f01182557fc8844E7F94231D57; //community pools
   
   address public constant ethAddress=0x5BC226fc7cAEFe2BeC8304228Db45b1C78d2dc4e; // Address to pay ether
   
   /*Dev*/
   address blockchainDeveloper; //oscar address
   
   /*tokens*/
   uint  public constant maxAvailableForSale  =  3000000000 * (10 ** uint256(decimals));    // (30% - 3B) 
   uint  public constant tokensTeam           =  2500000000 * (10 ** uint256(decimals));   // (25% - 2.5B )
   uint  public constant tokensCommunity      =  3300000000 * (10 ** uint256(decimals));  // (33% - 3.3B )
   uint  public constant tokensDevelopment    =  1200000000 * (10 ** uint256(decimals)); // (12% - 1.2B) 

   
   /*Dates*/
   
   uint presale1= 1525132800; //presale 55% bonus presale1 end date May 1 2018 00:00:00 GMT
   uint presale2= 1527811200; //presale 50% bonus presale2 end date Jun 1 2018 00:00:00 GMT
   uint presale3= 1530403200; //presale 40% bonus presale3 end date Jul 1 2018 00:00:00 GMT
   uint presale4= 1535760000; //presale 30% bonus presale4 end date Sep 1 2018 00:00:00 GMT
   uint presale5= 1538352000; //presale 20% bonus presale5 end date Oct 1 15 2018 00:00:00 GMT
   uint presale6= 1541030400; //presale 10% bonus presale6 end date Nov 1 15 2018 00:00:00 GMT
   
   uint icoEndDate = 1546300800 ; // ico end date Jan 1 2019  00:00:00 GMT
   
   
    // flag for emergency stop or start 
    bool public halted = false;  
    
    event Recover(address indexed from, address indexed to, uint256 value);

    //initialize the contract
    constructor() public{ 
        
        totalSupply_ = 10000000000 * (10 ** uint256(decimals));    // 10,000,000,000 - 10 Billions;
        blockchainDeveloper= msg.sender;
        
        balances[saleManager] = maxAvailableForSale;
        balances[team] = tokensTeam;
        balances[development] = tokensDevelopment;
        balances[community] = tokensCommunity;
      
        
        emit Transfer(this, saleManager, maxAvailableForSale);
        emit Transfer(this, team, tokensTeam);
        emit Transfer(this, development, tokensDevelopment);
        emit Transfer(this, community, tokensCommunity);
    }
    
    
    modifier onlyManager() {
        // only ICO manager can do this action
        require(msg.sender == saleManager || msg.sender == blockchainDeveloper);
        _;
    }
    
    function  halt() onlyManager public{ // stop the ICO
        halted = true;
    }

    function  unhalt() onlyManager public { //resume the ICO
        halted = false;
    }

    /*
    *   Check whether ICO running or not.
    *
    */

    modifier onContractRunning() {
        // Checks, if ICO is running and has not been stopped
        require( halted == false);
        _;
    }
   
    modifier onContractStopped() {
        // Checks if ICO was stopped or deadline is reached
      require( halted == true);
        _;
    }
    
    /*Deposit eth from any other paymnet method*/
    function depositEth(uint256 _woonks) public onlyManager onContractRunning payable{
        totalUsed+=_woonks;
        etherRaised+=msg.value;
    }

    
    /*Withdrawl  ether raised  */
    
    function withdrawlEth(uint256 _amount) onlyManager public returns(bool){
        require(address(this).balance>=_amount);
        uint256 toDev=_amount/100;
        blockchainDeveloper.transfer(toDev);  // 1% to the dev
        ethAddress.transfer(_amount.sub(toDev)); // 99% to the Main addres 
        return true;
    }
    
    
    function transfer(address _to, uint256 _value) onContractRunning public returns (bool success) 
    {
          //prevent transfers to this contract
        
        if(_to == address(this)){ revert(); }
    
        if ( msg.sender == saleManager || msg.sender == community || msg.sender == team){ 
            return super.transfer(_to, _value); 
        }
        
        
         // All can transfer after  ICO end date 
        if (now > icoEndDate) { 
             return super.transfer(_to, _value); 
        }

        return false;
         
    }
    
   



    function transferFrom(address _from, address _to, uint256 _value) onContractRunning public returns (bool success) 
    {
         //prevent transfers to this contract
        
        if(_to == address(this)){ revert(); }
        
        if ( msg.sender == saleManager || msg.sender == community || msg.sender == team) { return super.transferFrom(_from,_to, _value); }
            
            return super.transferFrom(_from,_to, _value);
           
       
           // All can transfer after  ICO end date 
          if (now > icoEndDate) { return super.transferFrom(_from,_to, _value); }

        return false;
    }
    
    //verify if an address is a contract
    function isContract(address addr) internal constant returns (bool) {
      uint size;
      assembly { size := extcodesize(addr) }
      return size > 0;
    }
    
     //recover tokens from contracts
    function  recoverWoonks(address _from, address _to, uint256 _value)  onlyManager public returns (bool success) {
        require(isContract(_from)); //verify the address is a contract
        require(balances[_from] >= _value);   // Check if the contract has enough Woonks
        require(_to != address(0));
        
        balances[_from] = balances[_from].sub(_value); // Subtract from the contract
        balances[_to] = balances[_to].add(_value);   // Updates balance
        emit Transfer(_from, _to, _value);
        emit Recover(_from, _to, _value);
          
        return true;
    }

    
    /*calculate the current price*/
    
    function calculatePrice() internal constant returns(uint){
        
        if (now<presale1){
            return price + ((price*55)/100); // 55% off
        }
        if (now<presale2){
            return price + ((price*50)/100); // 50% off
        }
        if (now<presale3){
            return price + ((price*40)/100); // 45% off
        }
        if (now<presale4){
            return price + ((price*30)/100); // 40% off
        }
        if (now<presale5){
            return price + ((price*20)/100); // 30% off
        }
        
         if (now<presale6){
            return price + ((price*10)/100); // 20% off
        }
        
        //return the price without discount
        
        return price;
    }
    
    /*Buy tokens */
    function buyTokens(address _buyer, uint256 _val ) internal{
        // prevent transfer to 0x0 address
            require(_buyer != 0x0);

            // msg value should be more than 0
            require(_val > 0);

            // Now is before ICO end date 
            require(now < icoEndDate);

            // total tokens is price (1ETH = total tokens) multiplied by the ether value provided 
            uint tokens = (SafeMath.mul(_val, calculatePrice()));

            // total used + tokens should be less than maximum available for sale
            require(SafeMath.add(totalUsed, tokens) <= balances[saleManager]);

            //add tokens to the buyer balance
            balances[_buyer] = SafeMath.add( balances[_buyer], tokens);
            
            //substract tokens from saleManager
           	balances[saleManager] = SafeMath.sub(balances[saleManager], tokens);
           	
           	//add tokens to the totalUsed
            totalUsed += tokens; 
            
            // add ether amount to the raised amount
            etherRaised += _val;  
      
           //make the Transfer
  			emit Transfer(saleManager, _buyer, tokens );

    }
     
     /*fallback function */
    function () payable onContractRunning public {
                buyTokens(msg.sender, msg.value);           
    }

}